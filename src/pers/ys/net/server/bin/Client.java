package pers.ys.net.server.bin;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

    public static void main(String[] args) {
        try {
            Socket client = new Socket("127.0.0.1", 5555);

            InputStream in = client.getInputStream();
            OutputStream out = new BufferedOutputStream(client.getOutputStream());

            byte[] head = new byte[4];
            int length = 0;
            int type = 0;
            byte[] body;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            do {
                in.read(head);
                length = (head[0] << 8 & 0xFF00) + (head[1] & 0xFF);
                type = (head[2] << 8 & 0xFF00) + (head[3] & 0xFF);
                body = new byte[length];
                int pos = 0;
                while (pos < length) {
                    pos += in.read(body, pos, length - pos);
                }
                if (type == 1) {
                    String msg = new String(body, "UTF-8");
                    System.out.print(msg);
                    msg = br.readLine();
                    body = msg.getBytes("UTF-8");
                    length = body.length;
                    head[0] = (byte) (length >> 8 & 0xFF);
                    head[1] = (byte) (length & 0xFF);
                    head[2] = 1 >> 8 & 0xFF;
                    head[3] = 1 & 0xFF;
                    out.write(head);
                    out.write(body);
                    out.flush();
                } else {
                    break;
                }
            } while (true);
            close(br);
            close(client);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void close(Closeable closeable) throws IOException {
        if (closeable != null) {
            closeable.close();
        }
    }
}
