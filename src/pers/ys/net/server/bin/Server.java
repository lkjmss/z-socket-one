package pers.ys.net.server.bin;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pers.ys.net.server.component.TypeHandler;

public class Server {

    private static final Log        LOGGER = LogFactory.getLog(Server.class);

    private static final int        PORT   = 5555;

    private static volatile boolean isRun  = true;

    public static void main(String[] args) {
        LOGGER.info("Server Start!");
        try {
            ServerSocket server = new ServerSocket(PORT);
            LOGGER.info("Start listen on port: " + PORT);
            while (isRun) {
                Socket client = server.accept();
                LOGGER.info("A client has been connected.");
                new Thread(new TypeHandler(client), "Server").start();
            }
            close(server);
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    public static void close(Closeable closeable) throws IOException {
        if (closeable != null) {
            closeable.close();
        }
    }
}
