package pers.ys.net.server.component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pers.ys.net.server.bin.Server;

public class TelnetHandler implements Runnable {

    private static final Log  LOGGER             = LogFactory.getLog(TelnetHandler.class);

    private static final long RECEIVE_TIME_DELAY = 10000;

    private final Socket      socket;

    private final InputStream in;

    private final PrintWriter writer;

    private long              lastReceiveTime    = System.currentTimeMillis();

    private boolean           enableTimeout      = false;

    public TelnetHandler(Socket socket) throws IOException {
        this.socket = socket;
        in = this.socket.getInputStream();
        OutputStream out = this.socket.getOutputStream();
        // PrintWriter的print需要调用刷新，设置autoFlush为true时println会自动刷新
        writer = new PrintWriter(out, true);

        // PrintStream无须设置autoFlush为true，所有方法都会自动刷新
        PrintStream ps = new PrintStream(out);
        ps.print("Welcome ys server!\r\n\r\nsend: ");
    }

    @Override
    public void run() {
        try {
            do {
                if (!handleMsg(read())) {
                    break;
                }
                lastReceiveTime = System.currentTimeMillis();
            } while (true);
            Server.close(socket);
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    private String read() throws IOException {
        StringBuilder sb = new StringBuilder();
        byte[] buff = new byte[1024];
        int length = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while ((length = in.read(buff, 0, buff.length)) > 0) {
            bos.reset();
            bos.write(buff, 0, length);
            sb.append(bos.toString());
            if (sb.toString().endsWith("\r\n")) {
                break;
            }
        }
        return sb.toString();
    }

    private boolean handleMsg(String msg) {
        if (enableTimeout && System.currentTimeMillis() - lastReceiveTime > RECEIVE_TIME_DELAY) {
            LOGGER.info("Client timeout.");
            return false;
        } else if (!"".equals(msg)) {
            LOGGER.info(msg);
            if (msg.endsWith("\r\n")) {
                msg = msg.substring(0, msg.length() - 2);
            }
            writer.print("\r\nServer: [" + msg + "]\r\nsend: ");
            writer.flush();
            return true;
        } else {
            LOGGER.info("Client exit.");
            return false;
        }
    }
}
