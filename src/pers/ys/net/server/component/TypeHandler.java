package pers.ys.net.server.component;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pers.ys.net.server.bin.Server;

public class TypeHandler implements Runnable {

    private static final Log   LOGGER = LogFactory.getLog(TypeHandler.class);

    private final Socket       socket;
    private final InputStream  in;
    private final OutputStream out;

    private boolean            enableTimeout;
    private long               receiveTimeDelay;
    private long               lastReceiveTime;

    private int                length;
    private int                type;
    private byte[]             body;

    public TypeHandler(Socket socket) throws IOException {
        this(socket, false);
    }

    public TypeHandler(Socket socket, boolean enableTimeout) throws IOException {
        this(socket, enableTimeout, 10000);
    }

    public TypeHandler(Socket socket, boolean enableTimeout, long receiveTimeDelay) throws IOException {
        this.socket = socket;
        in = this.socket.getInputStream();
        out = new BufferedOutputStream(this.socket.getOutputStream());
        this.enableTimeout = enableTimeout;
        this.receiveTimeDelay = receiveTimeDelay;
        lastReceiveTime = System.currentTimeMillis();
        sendString("Welcome ys server!\r\n\r\nsend: ");
    }

    @Override
    public void run() {
        try {
            do {
                if (receive() == -1) {
                    break;
                }
                if (type == 1) {
                    handleString(new String(body, "UTF-8"));
                }
            } while (true);
            Server.close(socket);
        } catch (IOException e) {
            LOGGER.error("client error", e);
        }
    }

    private int receive() throws IOException {
        if (enableTimeout && System.currentTimeMillis() - lastReceiveTime > receiveTimeDelay) {
            LOGGER.info("Client timeout.");
            return -1;
        }

        byte[] head = new byte[4];
        if (in.read(head) != head.length) {
            LOGGER.info("Client exit.");
            return -1;
        }

        length = (head[0] << 8 & 0xFF00) + (head[1] & 0xFF);
        type = (head[2] << 8 & 0xFF00) + (head[3] & 0xFF);
        body = new byte[length];

        int pos = 0;
        while (pos < length) {
            pos += in.read(body, pos, length - pos);
        }

        lastReceiveTime = System.currentTimeMillis();
        return length;
    }

    private void send(byte[] head, byte[] body) throws IOException {
        synchronized (out) {
            out.write(head);
            if (body != null) {
                out.write(body);
            }
            out.flush();
        }
    }

    private byte[] generateHead(int length, int type) {
        byte[] head = new byte[4];
        head[0] = (byte) (length >> 8 & 0xFF);
        head[1] = (byte) (length & 0xFF);
        head[2] = (byte) (type >> 8 & 0xFF);
        head[3] = (byte) (type & 0xFF);
        return head;
    }

    private void sendZero() throws IOException {
        send(generateHead(0, 0), null);
    }

    private void handleString(String msg) throws IOException {
        if (!"exit".equals(msg)) {
            LOGGER.info(msg);
            sendString("\r\nServer: [" + msg + "]\r\nsend: ");
        } else {
            sendZero();
        }
    }

    private void sendString(String msg) throws IOException {
        byte[] body = msg.getBytes("UTF-8");
        send(generateHead(body.length, 1), body);
    }
}
